﻿using System.Collections.Generic;

namespace Iterator
{
    public class BSTNode
    {
        public BSTNode left, right;
        public int value;

        public BSTNode(int value)
        {
            this.value = value;
            left = right = null;
        }
    }

    public class Tree
    {
        public BSTNode head;

        public Tree()
        {
            head = null;
        }

        public BSTIterator CreateIterator()
        {
            return new BSTIterator(this);
        }

        public void Add(int v)
        {
            if (head == null)
            {
                head = new BSTNode(v);
                return;
            }

            BSTNode p = head;
            while (true)
            {
                if (v < p.value)
                {
                    if (p.left == null)
                    {
                        p.left = new BSTNode(v);
                        return;
                    }
                    else p = p.left;
                }
                else
                {
                    if (p.right == null)
                    {
                        p.right = new BSTNode(v);
                        return;
                    }
                    else p = p.right;
                }
            }
        }
    }

    public class BSTIterator : IIterator<BSTNode> //BFS
    {
        Tree tree;
        BSTNode p;
        Queue<BSTNode> q;
        
        public BSTIterator(Tree t)
        {
            q = new Queue<BSTNode>();
            tree = t;
            p = tree.head;
            q.Enqueue(tree.head);
        }
        public BSTNode First()
        {
            return tree.head;
        }

        public bool hasNext()
        {
            return q.Count > 0;
        }

        public BSTNode Next()
        {
            if (hasNext() == true)
            {
                BSTNode retval = q.Dequeue();
                if (retval.left != null) q.Enqueue(retval.left);
                if (retval.right != null) q.Enqueue(retval.right);
                return retval;
            }
            else return null;
        }

        public void Restart()
        {
            p = tree.head;
            q.Clear();
            q.Enqueue(tree.head);
        }
    }
}
