﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Iterator
{
    class Program
    {
        static void Main(string[] args)
        {
            Tree t = new Tree();
            t.Add(5);
            t.Add(2);
            t.Add(45);
            t.Add(32);
            t.Add(1);
            t.Add(3);
            t.Add(150);

            IIterator<BSTNode> it = t.CreateIterator();

            printData(it);
        }

        private static void printData(IIterator<BSTNode> it)
        {
            while (it.hasNext() == true)
            {
                Console.WriteLine(it.Next().value);
            }
        }
    }
}
